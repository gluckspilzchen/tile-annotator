/*
    TileAnnotator is a utility program to add annotations to tiles of a tileset.
    Copyright (C) 2023-2024 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/

import ui.Components;
import ui.UISettings;
import tileAnnotator.Annotations.FieldType;
import tileAnnotator.Annotations.TypeDef;
import tileAnnotator.Annotations.Field;
import tileAnnotator.Annotations.ConstraintTypes;
import WidgetSettings.*;


class Form extends h2d.Object {
	var menu : tools.MenuControl;

	function makeIntField(update : (tileAnnotator.Field) -> Void, container : h2d.Object, f : Field) {
		var v = switch f {
			case null: "";
			case FInt (f): f + "";
			default: "";
		};

		var settings = getTextSettings();
		var textField = new ui.TextFieldComp(menu.control, settings, container);
		textField.text = v;

		var readInt = (text) -> {
			var v =	Std.parseInt(text);

			if (v == null) {
				textField.text = "";
				return;
			}

			update(FInt (v));
		};

		textField.onFocusLost = (text) -> {
			menu.unlock();
			menu.control.suspend(0.3);
			readInt(text);
		};
		menu.addItem(textField);

		return textField;
	}

	function makeFloatField(update : (tileAnnotator.Field) -> Void, container : h2d.Object, f : Field) {
		var v = switch f {
			case null: "";
			case FFloat (f): f + "";
			default: "";
		};

		var settings = getTextSettings();
		var textField = new ui.TextFieldComp(menu.control, settings, container);
		textField.text = v;

		var readFloat = (text) -> {
			var v =	Std.parseFloat(text);

			var v =
				if (Math.isNaN(v))
					null;
				else
					FFloat (v);

			if (v == null) {
				textField.text = "";
				return;
			}

			update(v);
		};

		textField.onFocusLost = (text) -> {
			menu.unlock();
			menu.control.suspend(0.3);
			readFloat(text);
		};
		menu.addItem(textField);

		return textField;
	}

	function makeBoolField(update : (tileAnnotator.Field) -> Void, container : h2d.Object, f : Field) {
		var v = switch f {
			case null: false;
			case FBool (b): b;
			default: false;
		};

		var settings = getCBSettings();
		var b = new ui.CheckboxComp(settings, container);
		b.setIsChecked(v);
		b.label = b.isChecked ? "X" : "";
		b.onClick = function () {
			b.setIsChecked(!b.isChecked);
			b.label = b.isChecked ? "X" : "";
			update(FBool (b.isChecked));
		}
		b.onOut = function() {
			update(FBool (b.isChecked));
		}
		menu.addItem(b);
	}

	function makeEnumField(enums : Map<String, Array<String>>, update : (tileAnnotator.Field) -> Void, container : h2d.Object, name : String, v : Field) {
		var v = switch v {
			case null: "";
			case FEnum (_, s): s;
			default: "";
		};

		var settings = getTextSettings();
		var b = new ui.ButtonComp(settings, container);
		b.label = v;

		var values = enums.get(name);

		var options = new Array();
		for (v in values) {
			var item = {
				name : v,
				onClick : () -> {
					b.label = v;
					update(FEnum(name, v));
				}
			};
			options.push(item);
		}

		b.onClick = () -> {
			var menu = ui.Menu.vertical({ menu : menu, name : name, getWidgetSettings : getTextSettings, root : Tool.ME.uiRoot }, options);
			Tool.ME.recenter(menu);
		}
		menu.addItem(b);
	}

	function makeRecordField(level : Int, constraintTypes : ConstraintTypes<FieldType>, container : h2d.Object, model : Map<String, FieldType>, f : Field) {
		var fields = switch f {
			case FRecord (_, vs): vs;
			default: null;
		};

		for (kv in model.keyValueIterator()) {
			var container = new h2d.Flow(container);
			container.layout = Vertical;
			var line = new h2d.Flow(container);
			for (i in 0...level) {
				new h2d.Bitmap(h2d.Tile.fromColor(0x0, 20, 1, 0), line);
			}
			if (level == 0) {
				var settings = getDeleteSettings();
				var b = new ui.ButtonComp(settings, line);
				b.label = "X";
				b.onClick = () -> {
					model.remove(kv.key);
					container.remove();
				}
				menu.addItem(b);
			}
			var l = new h2d.Text(hxd.res.DefaultFont.get(), line);
			l.text = kv.key;
			var v = fields.get(kv.key);
			switch (kv.value) {
				case TInt:
					makeIntField(fields.set.bind(kv.key, _), line, v);

				case TFloat:
					makeFloatField(fields.set.bind(kv.key, _), line, v);

				case TBool:
					makeBoolField(fields.set.bind(kv.key, _), line, v);

				case TEnum(n):
					makeEnumField(constraintTypes.enums, fields.set.bind(kv.key, _), line, n, v);

				case TRecord(n):
					if (v == null) {
						v = FRecord (n, new Map());
						fields.set(kv.key, v);
					}

					makeRecordField(level+1, constraintTypes, container, constraintTypes.records.get(n), v);
			}
		}
	}

	public function new(menu : tools.MenuControl, dataModel : Map<String,FieldType>, data : Map<String, Field>, constraintTypes : ConstraintTypes<FieldType>, ?parent) {
		super(parent);

		this.menu = menu;

		var container = new h2d.Flow(this);
		container.layout = Vertical;

		makeRecordField(0, constraintTypes, container, dataModel, FRecord("", data));

		var bounds = getBounds();
		y = -bounds.height/2;
	}
}
