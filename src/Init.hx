/*
    TileAnnotator is a utility program to add annotations to tiles of a tileset.
    Copyright (C) 2023-2024 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/

import hxd.Key;
import tools.Action;
import tools.CustomControl;

class CustomControl extends tools.CustomControl {
	public function getActionName(a) {
		return switch (a) {
			case AddConstraintType: "Add constraint type";
			case AddField: "Add field";
			case ScaleUp : "Scale up";
			case ScaleDown : "Scale down";
			case Up : "Up";
			case Down : "Down";
			case Left : "Left";
			case Right : "Right";
			case Select : "Select";
			case Exit : "Exit";
			case Load : "Load";
			case Save : "Save";
		}
	}

	public function getSuspendDuration() {
		return 0.1;
	}

	public function suspend(d : Float) {
		Init.ME.editController.suspend(d);
	}
}

class Init extends dn.Process {
	public static var ME : Init;
	private var uiScene(get, never) : h2d.Scene; inline function get_uiScene() return Main.ME.uiScene;

	public var editController : tools.Controller;
	public var eca : dn.legacy.Controller.ControllerAccess;

	public function new() {
		super();
		ME = this;
		initControllers();

		delayer.addF( () -> { new Tool(); }, 1 );
	}

	function initControllers() {
		editController = new tools.Controller(uiScene);
		eca = editController.createAccess("main");

		initEditControllerBindings();
	}

	public function initEditControllerBindings() {
		tools.CustomControl.addBinding(Up, { modifiers : new Array(), key : tools.CustomControl.key(Key.UP), protected : true });
		tools.CustomControl.addBinding(Up, { modifiers : new Array(), key : tools.CustomControl.key(Key.K), protected : false });
		tools.CustomControl.addBinding(Down, { modifiers : new Array(), key : tools.CustomControl.key(Key.DOWN), protected : true });
		tools.CustomControl.addBinding(Down, { modifiers : new Array(), key : tools.CustomControl.key(Key.J), protected : false });
		tools.CustomControl.addBinding(Left, { modifiers : new Array(), key : tools.CustomControl.key(Key.LEFT), protected : true });
		tools.CustomControl.addBinding(Left, { modifiers : new Array(), key : tools.CustomControl.key(Key.H), protected : false });
		tools.CustomControl.addBinding(Right, { modifiers : new Array(), key : tools.CustomControl.key(Key.RIGHT), protected : true });
		tools.CustomControl.addBinding(Right, { modifiers : new Array(), key : tools.CustomControl.key(Key.L), protected : false });
		tools.CustomControl.addBinding(Select, { modifiers : new Array(), key : tools.CustomControl.key(Key.ENTER), protected : true });
		tools.CustomControl.addBinding(Exit, { modifiers : new Array(), key : tools.CustomControl.key(Key.ESCAPE), protected : true });
		tools.CustomControl.addBinding(AddConstraintType, { modifiers : new Array(), key : tools.CustomControl.key(Key.G), protected : false });
		tools.CustomControl.addBinding(AddField, { modifiers : new Array(), key : tools.CustomControl.key(Key.F), protected : false });
		tools.CustomControl.addBinding(ScaleUp, { modifiers : new Array(), key : tools.CustomControl.key(Key.PGUP), protected : false });
		tools.CustomControl.addBinding(ScaleDown, { modifiers : new Array(), key : tools.CustomControl.key(Key.PGDOWN), protected : false });
		tools.CustomControl.addBinding(Load, { modifiers : new Array(), key : tools.CustomControl.key(Key.E), protected : false });
		tools.CustomControl.addBinding(Save, { modifiers : new Array(), key : tools.CustomControl.key(Key.W), protected : false });
	}
}
