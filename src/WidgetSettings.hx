/*
    TileAnnotator is a utility program to add annotations to tiles of a tileset.
    Copyright (C) 2023-2024 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
class WidgetSettings {
	static var maxWidth = 100;
	static public function getTextSettings() {
		var w = maxWidth;
		var h = 20;
		var baseTile = new h2d.SpriteBatch(h2d.Tile.fromColor(0x425e9e, w, h, 1));
		baseTile.add(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0x425e9e, w, h, 1)));
		var hoverTile = new Array();
		hoverTile.push(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0x425e9e, w, h, 1)));
		hoverTile.push(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0xeeeeee, w, h, 0.3)));
		var activeTile = new h2d.SpriteBatch(h2d.Tile.fromColor(0x425e9e, w, h, 1));
		activeTile.add(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0x425e9e, w, h, 1)));
		activeTile.add(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0xeeeeee, w, h, 0.3)));
		return {
			width : w,
			height : h,
			hover : hoverTile,
			active : activeTile,
			base : baseTile,
		};
	}

	static public function getDeleteSettings() {
		var w = 20;
		var h = 20;
		var baseTile = new h2d.SpriteBatch(h2d.Tile.fromColor(0x425e9e, w, h, 1));
		baseTile.add(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0x425e9e, w, h, 1)));
		var hoverTile = new Array();
		hoverTile.push(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0x425e9e, w, h, 1)));
		hoverTile.push(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0xeeeeee, w, h, 0.3)));
		var activeTile = new h2d.SpriteBatch(h2d.Tile.fromColor(0x425e9e, w, h, 1));
		activeTile.add(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0x425e9e, w, h, 1)));
		activeTile.add(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0xeeeeee, w, h, 0.3)));
		return {
			width : w,
			height : h,
			hover : hoverTile,
			active : activeTile,
			base : baseTile,
		};
	}

	static public function getCBSettings() {
		var w = 20;
		var h = 20;
		var baseTile = new h2d.SpriteBatch(h2d.Tile.fromColor(0x425e9e, w, h, 1));
		baseTile.add(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0x425e9e, w, h, 1)));
		var hoverTile = new Array();
		hoverTile.push(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0x425e9e, w, h, 1)));
		hoverTile.push(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0xeeeeee, w, h, 0.3)));
		var activeTile = new h2d.SpriteBatch(h2d.Tile.fromColor(0x425e9e, w, h, 1));
		activeTile.add(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0x425e9e, w, h, 1)));
		return {
			width : w,
			height : h,
			hover : hoverTile,
			active : activeTile,
			base : baseTile,
		};
	}
}
