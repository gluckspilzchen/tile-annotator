/*
    TileAnnotator is a utility program to add annotations to tiles of a tileset.
    Copyright (C) 2023-2024 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/

class Main extends hxd.App {
	public static var ME : Main;

	public var objectScene : h2d.Scene;
	public var renderTarget : h3d.mat.Texture;

	private var bg : h2d.Bitmap;

	public var uiScene(get, never) : h2d.Scene; inline function get_uiScene() return s2d;

	// Boot
	static function main() {
		new Main();
	}

	// Engine ready
	override function init() {
		ME = this;

        hxd.Res.initEmbed();
		Settings.init();

		objectScene = new h2d.Scene();
		renderTarget = new h3d.mat.Texture(engine.width, engine.height, [ Target ]);

		bg = new h2d.Bitmap(h2d.Tile.fromTexture(renderTarget));
		uiScene.add(bg, 0);

		onResize();

		new Init();
	}

	override function onResize() {
		super.onResize();
		dn.Process.resizeAll();

		objectScene.checkResize();
		uiScene.checkResize();

		renderTarget.resize(engine.width, engine.height);

		bg.tile.scaleToSize(engine.width, engine.height);
	}

	override function dispose() {
		super.dispose();

		if (uiScene != null)
			uiScene.dispose();
	}

	override function update(deltaTime:Float) {
		super.update(deltaTime);

		dn.legacy.Controller.beforeUpdate();
		dn.Process.updateAll(hxd.Timer.tmod);
	}

	override function render (e:h3d.Engine) {

		engine.pushTarget(renderTarget);

		engine.clear(0, 1);
		objectScene.render(e);
		engine.popTarget();

		uiScene.render(e);
	}
}
