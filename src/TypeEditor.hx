/*
    TileAnnotator is a utility program to add annotations to tiles of a tileset.
    Copyright (C) 2023-2024 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
import ui.Components;
import tileAnnotator.Annotations.ConstraintTypes;
import tileAnnotator.Annotations.FieldType;
import WidgetSettings.*;

class TypeEditor extends h2d.Object {
	var windowW : Float;
	var windowH : Float;

	var display : h2d.Object;
	var uiRoot : h2d.Object;

	var menu : tools.MenuControl.GridMenuControl;

	public function new(parentMenu : tools.MenuControl, w, h, ?parent) {
		super(parent);

		this.menu = new tools.MenuControl.GridMenuControl(parentMenu.control, "Type editor");
		this.menu.register(parentMenu);

		windowW = w;
		windowH = h;

		var mask = new h2d.Bitmap(h2d.Tile.fromColor(0x0, 1, 1, 1), this);

		display = new h2d.Object(this);
		uiRoot = new h2d.Object(this);

		var b = new ui.ButtonComp(getTextSettings(), uiRoot);
		menu.addItem(2, b);
		b.label = "Annotations";
		b.onClick = this.close;
		var bounds = b.getBounds();
		b.x = 0.95 * w - bounds.width;
		b.y = 0.05 * h - bounds.height;

		mask.scaleX = dn.M.ceil(w);
		mask.scaleY = dn.M.ceil(h);
		visible = false;
	}

	function drawInterface(types : ConstraintTypes<FieldType>) {
		display.removeChildren();
		menu.clear(0);
		menu.clear(1);
		var container = new h2d.Object(display);

		var enums = new h2d.Flow(container);
		enums.layout = Vertical;
		enums.horizontalAlign = Left;
		enums.verticalAlign = Top;
		enums.x = 0.1*windowW;
		enums.y = 0.1*windowH;
		var l = new h2d.Text(hxd.res.DefaultFont.get(), enums);
		l.text = "Enums:";
		for (e in types.enums.keyValueIterator()) {
			var header = new h2d.Flow(enums);
			header.layout = Horizontal;
			var b = new ui.ButtonComp(getDeleteSettings(), header);
			b.label = "X";
			b.onClick = () -> {
				types.enums.remove(e.key);
				drawInterface(types);
			}
			menu.addItem(0, b);
			var l = new h2d.Text(hxd.res.DefaultFont.get(), header);
			l.text = e.key;
			var l = new h2d.Text(hxd.res.DefaultFont.get(), header);
			l.text = " =";
			for (c in e.value) {
				var constructor = new h2d.Flow(enums);
				constructor.layout = Horizontal;
				new h2d.Bitmap(h2d.Tile.fromColor(0x0, 20, 1, 0), constructor);
				var b = new ui.ButtonComp(getDeleteSettings(), constructor);
				b.label = "X";
				b.onClick = () -> {
					e.value.remove(c);
					drawInterface(types);
				}
				menu.addItem(0, b);
				var l = new h2d.Text(hxd.res.DefaultFont.get(), constructor);
				l.text = "| ";
				var text = new h2d.Text(hxd.res.DefaultFont.get(), constructor);
				text.text = c;
			}
			var line = new h2d.Flow(enums);
			new h2d.Bitmap(h2d.Tile.fromColor(0x0, 20, 1, 0), line);
			var b = new ui.ButtonComp(getDeleteSettings(), line);
			b.label = "+";
			b.onClick = () -> {
				var menu =
					ui.UserInput.readString(
						{ name : "New enum constructor:"
						, menu : menu
						, root : uiRoot
						, getWidgetSettings : getTextSettings },
						(name) -> {
							e.value.push(name);
							drawInterface(types);
						}
				);

				Tool.ME.recenter(menu);
			}
			menu.addItem(0, b);
		}

		var records = new h2d.Flow(container);
		records.layout = Vertical;
		records.horizontalAlign = Left;
		records.verticalAlign = Top;
		records.x = 0.6*windowW;
		records.y = 0.1*windowH;
		var l = new h2d.Text(hxd.res.DefaultFont.get(), records);
		l.text = "Records:";
		for (r in types.records.keyValueIterator()) {
			var header = new h2d.Flow(records);
			header.layout = Horizontal;
			var b = new ui.ButtonComp(getDeleteSettings(), header);
			b.label = "X";
			b.onClick = () -> {
				types.records.remove(r.key);
				drawInterface(types);
			}
			var l = new h2d.Text(hxd.res.DefaultFont.get(), header);
			l.text = r.key;
			menu.addItem(1, b);
			var l = new h2d.Text(hxd.res.DefaultFont.get(), header);
			l.text = "= {";
			for (f in r.value.keyValueIterator()) {
				var field = new h2d.Flow(records);
				field.layout = Horizontal;
				new h2d.Bitmap(h2d.Tile.fromColor(0x0, 20, 1, 0), field);
				var b = new ui.ButtonComp(getDeleteSettings(), field);
				b.label = "X";
				b.onClick = () -> {
					r.value.remove(f.key);
					drawInterface(types);
				}
				menu.addItem(1, b);
				var text = new h2d.Text(hxd.res.DefaultFont.get(), field);
				text.text = f.key;
				var l = new h2d.Text(hxd.res.DefaultFont.get(), field);
				l.text = " : ";
				var b = new ui.ButtonComp(getTextSettings(), field);
				b.label = typeToString(f.value);
				b.onClick = () -> addField(types, r.value, f.key);
				menu.addItem(1, b);
			}
			var line = new h2d.Flow(records);
			new h2d.Bitmap(h2d.Tile.fromColor(0x0, 20, 1, 0), line);
			var b = new ui.ButtonComp(getDeleteSettings(), line);
			b.label = "+";
			b.onClick = () -> readField(types, r.value);
			menu.addItem(1, b);
			var l = new h2d.Text(hxd.res.DefaultFont.get(), records);
			l.text = "}";
		}
	}

	private function addField(constraintTypes : ConstraintTypes<FieldType>, fields : Map<String, FieldType>, text) {
		var options = [
		{ name : "Int", onClick : () -> { fields.set(text, TInt); drawInterface(constraintTypes); } },
		{ name : "Float", onClick : () -> { fields.set(text, TFloat); drawInterface(constraintTypes); } },
		{ name : "Bool", onClick : () -> { fields.set(text, TBool); drawInterface(constraintTypes); } }
		];
		for (e in constraintTypes.enums.keys()) {
			options.push({ name : e, onClick : () -> { fields.set(text, TEnum (e)); drawInterface(constraintTypes); } });
		}
		for (r in constraintTypes.records.keys()) {
			options.push({ name : r, onClick : () -> { fields.set(text, TRecord (r)); drawInterface(constraintTypes); } });
		}

		var menu = ui.Menu.vertical({ menu : menu, name : "Add field", getWidgetSettings : getTextSettings, root : display }, options);
		Tool.ME.recenter(menu);
	}

	private function readField(constraintTypes : ConstraintTypes<FieldType>, fields : Map<String, FieldType>) {
		var o = ui.UserInput.readString({ root : uiRoot, name : "New record field name:", getWidgetSettings : getTextSettings, menu : menu }, addField.bind(constraintTypes, fields, _));
		Tool.ME.recenter(o);
	}

	static public function typeToString(t : FieldType) {
		return
			switch t {
				case TInt : "Int";
				case TFloat : "Float";
				case TBool : "Bool";
				case (TEnum (n) | TRecord (n)) : n;
			}
	}

	public function open(types : ConstraintTypes<FieldType>) {
		visible = true;
		menu.unroll();

		drawInterface(types);
	}

	public function close() {
		visible = false;
		menu.rollBack();

		display.removeChildren();
	}
}
