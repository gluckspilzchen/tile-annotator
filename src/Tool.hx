/*
    TileAnnotator is a utility program to add annotations to tiles of a tileset.
    Copyright (C) 2023-2024 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
import ui.Components;
import sys.io.File;
import tileAnnotator.Annotations.Coord;
import tileAnnotator.Annotations.Field;
import tileAnnotator.Annotations.FieldType;
import tileAnnotator.Annotations.TypeDef;
import tileAnnotator.Annotations.TileAnnotations;
import tileAnnotator.Annotations.Annotation;
import tileAnnotator.Annotations.ConstraintTypes;
import ui.Menu;
import WidgetSettings;

typedef Data = {
	var x : Int;
	var y : Int;
	var hook : h2d.Object;
	var	tile : h2d.Tile;
	var highlight : h2d.Graphics;
	var annotations : Map<String, Field>;
}

class Tool extends dn.Process {
	public static var ME : Tool;

	private var eca(get, never) : dn.legacy.Controller.ControllerAccess; inline function get_eca() return Init.ME.eca;
	private var uiScene(get, never) : h2d.Scene; inline function get_uiScene() return Main.ME.uiScene;
	public var uiRoot(default, null) : h2d.Object;

	private var tiles : Array<Data>;
	private var current : Int;

	var control : Init.CustomControl;
	var menu : tools.MenuControl.VerticalMenuControl;

	public var scroller : h2d.Object;
	var tileDisplay : h2d.Object;
	var annotationDisplay : h2d.Object;
	var form : Form;
	var typeEditor : TypeEditor;
	var camera : Camera;

	var constraintTypes : ConstraintTypes<FieldType>;
	var annotations : Map<String, FieldType>;

	var typesButton : ui.ButtonComp;

	var filename : String;
	var tileSize : Int;

	public function new() {
		super();
		ME = this;

		tileSize = 0;
		constraintTypes = { enums : new Map(), records : new Map() };
		annotations = new Map();

		control = new Init.CustomControl(eca);
		menu = new tools.MenuControl.VerticalMenuControl(control, "Tool");

		tiles = new Array();
		current = 0;

		createRootInLayers(Main.ME.objectScene, 1);

		scroller = new h2d.Object(root);
		tileDisplay = new h2d.Object(root);
		uiRoot = new h2d.Object();
		uiScene.add(uiRoot, 1);
		annotationDisplay = new h2d.Object(uiRoot);
		annotationDisplay.x = 0.05 * w();
		annotationDisplay.y = h()/2;

		form = new Form(menu, annotations, new Map(), constraintTypes, annotationDisplay);
		typeEditor = new TypeEditor(menu, w(), h());
		uiScene.add(typeEditor, 2);

		var b = new ui.ButtonComp(getTextSettings());
		menu.addItem(b);
		uiScene.add(b, 1);
		b.label = "Types";
		b.onClick = () -> {
			  typeEditor.open(constraintTypes);
		}
		var bounds = b.getBounds();
		b.x = 0.95 * w() - bounds.width;
		b.y = 0.05 * h() - bounds.height;
		typesButton = b;

		camera = new Camera();
	}

	static var maxWidth = 100;
	private function getTextSettings() {
		var w = maxWidth;
		var h = 20;
		var baseTile = new h2d.SpriteBatch(h2d.Tile.fromColor(0x425e9e, w, h, 1));
		baseTile.add(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0x425e9e, w, h, 1)));
		var hoverTile = new Array();
		hoverTile.push(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0x425e9e, w, h, 1)));
		hoverTile.push(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0xeeeeee, w, h, 0.3)));
		var activeTile = new h2d.SpriteBatch(h2d.Tile.fromColor(0x425e9e, w, h, 1));
		activeTile.add(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0x425e9e, w, h, 1)));
		activeTile.add(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0xeeeeee, w, h, 0.3)));
		return {
			width : w,
			height : h,
			hover : hoverTile,
			active : activeTile,
			base : baseTile,
		};
	}

	private function getUISettings(name) {
		return { menu : cast(menu, tools.MenuControl.Menu), name : name, root : uiRoot, getWidgetSettings : getTextSettings };
	}

	function readAseprite(file:String): aseprite.Aseprite {
		var bytes = File.getBytes(file);
		return aseprite.Aseprite.fromBytes(bytes);
	}

	function extractTiles(name: String, loadData) {
		var size = tileSize;
		var container = new h2d.Object(scroller);
		container.y = 0.9 * h()- size;

		var ase = readAseprite(name);
		var allTiles = ase.toTiles();

		var offset = 0;
		var t = allTiles[0];
		for (j in 0...Math.floor(ase.height/size)) {
			for (i in 0...Math.floor(ase.width/size)) {
				var nt = t.sub(i * size, j * size, size, size);
				var hook = new h2d.Layers(container);

				var color = 0xffffff;
				var g = new h2d.Graphics(hook);
				g.beginFill(color, 0.);
				g.lineStyle(2, color);
				g.drawRect(0, 0, size, size);
				g.endFill();
				g.visible = false;

				var b = new h2d.Bitmap(nt);
				hook.addChildAt(b, 1);
				hook.x = offset;
				offset += Math.floor(size * 1.1);
				var annotations = loadData.get(XY(i,j));
				if (null == annotations) {
					annotations = new Map();
				}
				tiles.push({ x : i, y : j, hook : hook, tile : nt, highlight : g, annotations : annotations});
			}
		}

		updateCurrent();
	}

	function updateCurrent() {
		if (tiles.length == 0)
			return;

		tileDisplay.removeChildren();

		camera.trackTarget(tiles[current].hook, false);
		tiles[current].highlight.visible = true;
		var b = new h2d.Bitmap(tiles[current].tile, tileDisplay);

		refreshForm();

		menu.over();
		recenter(tileDisplay);
	}

	function refreshForm() {
		menu.clear();
		annotationDisplay.removeChildren();
		form = new Form(menu, annotations, tiles[current].annotations, constraintTypes, annotationDisplay);
		menu.addItem(typesButton);
	}

	function loadFile(n) {
		var r = ~/\.aseprite/;
		filename = r.replace(n, "");
		var loadData = load();

		if (0 != tileSize) {
			extractTiles(n, loadData);
		} else {
			var readFile = (text) -> {
				var size = Std.parseInt(text);
				if (null == size)
					return;
				tileSize = size;
				extractTiles(n, loadData);
			}

			var o = ui.UserInput.readString(getUISettings("Tile size:"), readFile);
			recenter(o);
		}
	}

	function openFile() {
		var files = sys.FileSystem.readDirectory(".");
		var r = ~/.*\.aseprite/;
		var imgs = files.filter(r.match);

		var options = new Array();

		for (n in imgs) {
			options.push({ name : n, onClick : () -> { loadFile(n); }});
		}

		var menu = Menu.vertical({ menu : menu, name : "Open file", getWidgetSettings : getTextSettings, root : uiRoot }, options);
		recenter(menu);
	}


	override function update() {
		super.update();

		switch (menu.sub) {
			case Some (_) :
				menu.update();
				return;
			case None:
		}

		if ( control.probe(Right) ) {
			tiles[current].highlight.visible = false;
			current++;

			if (current >= tiles.length) {
				current = 0;
			}

			updateCurrent();
		}

		if ( control.probe(Left) ) {
			tiles[current].highlight.visible = false;
			current--;

			if (current < 0) {
				current = tiles.length-1;
			}

			updateCurrent();
		}

		menu.update();

		if (control.probe(Save)) {
			save();
		}

		if (control.probe(AddConstraintType)) {
			readConstraintType();
		}

		if (control.probe(AddField)) {
			readField();
		}

		if (control.probe(ScaleUp)) {
			tileDisplay.scaleX += 1;
			tileDisplay.scaleY += 1;

			recenter(tileDisplay);
		}

		if (control.probe(ScaleDown)) {
			tileDisplay.scaleX -= 1;
			tileDisplay.scaleY -= 1;

			if (tileDisplay.scaleX < 1) {
				tileDisplay.scaleX = 1;
			}

			if (tileDisplay.scaleY < 1) {
				tileDisplay.scaleY = 1;
			}
			recenter(tileDisplay);
		}

		if (control.probe(Load)) {
			openFile();
		}
	}

	function setNewType<T> (name : String, constraintTypes : Map<String, T>, def : T) {
		if (constraintTypes.get(name) != null)
			return;

		constraintTypes.set(name, def);
	}

	function readConstraintType() {
		var addConstraintType = (text) -> {
			var typeOptions = [
				{ name : "Enum", onClick : () -> { setNewType(text, constraintTypes.enums, new Array()); } },
				{ name : "Record", onClick : () -> { setNewType(text, constraintTypes.records, new Map()); } }
			];

			var menu = Menu.vertical({ menu : menu, name : "New constraint", getWidgetSettings : getTextSettings, root : uiRoot}, typeOptions);
			recenter(menu);
		};

		var o = ui.UserInput.readString(getUISettings("New constraint name:"), addConstraintType);
		recenter(o);
	}

	function readField() {
		var addField = (text) -> {
			var options = [ {
				name : "Int",
				onClick : function() {
					annotations.set(text, TInt);
					refreshForm();
				}
			}, {
				name : "Float",
				onClick : function() {
					annotations.set(text, TFloat);
					refreshForm();
				}
			}, {
				name : "Bool",
				onClick : function() {
					annotations.set(text, TBool);
					refreshForm();
				}
			} ];
			for (e in constraintTypes.enums.keys()) {
				options.push({
					name : e,
					onClick : function() {
						annotations.set(text, TEnum (e));
						refreshForm();
					}
				});
			}

			for (r in constraintTypes.records.keys()) {
				options.push({
					name : r,
					onClick : function() {
						annotations.set(text, TRecord (r));
						refreshForm();
					}
				});
			}

			var menu = Menu.vertical(getUISettings("New field:"), options);
			recenter(menu);
		};

		var o = ui.UserInput.readString(getUISettings("New field name:"), addField);
		recenter(o);
	}

	function convertAnnotations(annotations : Map<String, FieldType>, tannotations : Map<String, Field>) {
		var a = new Map();
		for (i in annotations.keyValueIterator()) {
			var v : Annotation =
				switch tannotations.get(i.key) {
					case null: null;
					case FInt (i): {
						type : TypeEditor.typeToString(TInt),
						value : i + ""
					};
					case FFloat (f): {
						type : TypeEditor.typeToString(TFloat),
						value : f + ""
					};
					case FBool (b): {
						type : TypeEditor.typeToString(TBool),
						value : b + ""
					};
					case FEnum (n, s) : {
						type : TypeEditor.typeToString(TEnum (n)),
						value : s
					};
					case FRecord (n, vs) : {
						type : TypeEditor.typeToString(TRecord (n)),
						value : convertAnnotations(constraintTypes.records.get(n), vs)
					};
				}

			if (null == v)
				continue;

			a.set(i.key, v);
		}

		return a;
	}

	function save() {
		if (null == filename)
			return;

		var ts : Array<TileAnnotations> = new Array();

		for (t in tiles) {
			var a = convertAnnotations(annotations, t.annotations);

			ts.push({ x : t.x, y : t.y, annotations : a });
		}

		var records = new Map();
		for (r in constraintTypes.records.keyValueIterator()) {
			var fields = new Map();
			for (f in r.value.keyValueIterator()) {
				fields.set(f.key, TypeEditor.typeToString(f.value));
			}
			records.set(r.key, fields);
		}

		var types = { enums : constraintTypes.enums, records : records }

		var annots = new Map();
		for (a in annotations.keyValueIterator()) {
			annots.set(a.key, TypeEditor.typeToString(a.value));
		}

		tileAnnotator.Annotations.Utils.save(
				{ types : types
				, tiles : ts
				, tileSize : tileSize
				, annotations : annots
				},
				filename);
	}

	function load() {
		var data = tileAnnotator.Annotations.Utils.load(filename);

		if (null == data)
			return new Map();

		constraintTypes = data.types;
		annotations = data.annotations;
		tileSize = data.tileSize;

		return data.tiles;
	}

	public function recenter(o : h2d.Object) {
		var b = o.getBounds();
		o.x = w()/2 - b.width/2;
		o.y = h()/2 - b.height/2;
	}
}
