/*
    TileAnnotator is a utility program to add annotations to tiles of a tileset.
    Copyright (C) 2023-2024 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/

import dn.M;

class Camera extends dn.Process {
	public var target : Null<h2d.Object>;
	public var x : Float;
	public var y : Float;
	public var dx : Float;
	public var dy : Float;

	public function new() {
		super(Tool.ME);
		x = y = 0;
		dx = dy = 0;
	}

	public function trackTarget(e:h2d.Object, immediate:Bool) {
		target = e;
		if( immediate )
			recenter();
	}

	public inline function stopTracking() {
		target = null;
	}

	public function recenter() {
		if( target!=null ) {
			var p = target.localToGlobal();
			x = p.x - Tool.ME.scroller.x;
			y = p.y - Tool.ME.scroller.y;
		}
	}

	override function update() {
		super.update();

		// Follow target entity
		if( target!=null ) {
			var p = target.localToGlobal();

			var s = 0.006;
			var deadZone = 5;
			var tx = p.x - Tool.ME.scroller.x;
			var ty = p.y - Tool.ME.scroller.y;

			var d = M.dist(x,y, tx, ty);
			if( d>=deadZone ) {
				var a = Math.atan2( ty-y, tx-x );
				dx += Math.cos(a) * (d-deadZone) * s * tmod;
			}
		}

		var frict = 0.89;
		x += dx*tmod;
		dx *= Math.pow(frict,tmod);
	}

	override function postUpdate() {
		super.postUpdate();

		var scroller = Tool.ME.scroller;

		// Update scroller
		scroller.x = -x + w()*0.5;
	}
}
